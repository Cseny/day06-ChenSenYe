import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";
    public static final String DEFAULT_SPLIT_DELIMITER = "\\s+";
    public static final String DEFAULT_JOIN_DELIMITER = "\n";

    public String getResult(String inputStr) {
        String[] inputStrArray = splitStringToArray(inputStr);
        if (inputStrArray.length == 1) {
            return inputStr + " 1";
        } else {
            return tryGenerateResult(inputStrArray);
        }
    }

    private String tryGenerateResult(String[] inputStrArray) {
        try {
            List<WordInfo> inputWordList = generateInputWordList(inputStrArray);
            Map<String, List<WordInfo>> inputWordMap = generateInputWordMap(inputWordList);
            List<WordInfo> finalInputWordAndCountList = generateFinalInputWordAndCountList(inputWordMap);
            return generateResult(finalInputWordAndCountList);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private String generateResult(List<WordInfo> finalInputWordAndCountList) {
        return finalInputWordAndCountList.stream()
                .map(word -> word.getValue() + " " + word.getWordCount())
                .collect(Collectors.joining(DEFAULT_JOIN_DELIMITER));
    }

    private List<WordInfo> generateFinalInputWordAndCountList(Map<String, List<WordInfo>> inputWordMap) {
        return inputWordMap.entrySet()
                .stream()
                .map(entry -> new WordInfo(entry.getKey(), entry.getValue().size()))
                .sorted(Comparator.comparingInt(WordInfo::getWordCount).reversed())
                .collect(Collectors.toList());
    }

    private List<WordInfo> generateInputWordList(String[] inputStrArray) {
        return Arrays.stream(inputStrArray)
                .map(str -> new WordInfo(str, 1))
                .collect(Collectors.toList());
    }

    private String[] splitStringToArray(String inputStr) {
        return inputStr.split(DEFAULT_SPLIT_DELIMITER);
    }

    private Map<String, List<WordInfo>> generateInputWordMap(List<WordInfo> inputList) {
        return inputList.stream().collect(Collectors.groupingBy(WordInfo::getValue));
    }


}
