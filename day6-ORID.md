# O

- Code review.
- Session report.
- Refactor code.


# R

This is a fruitful day.

# I

- I have a certain understanding of the difference between the Strategy pattern and the Command pattern, but it is still relatively vague. I will consult the data myself later for in-depth understanding.

# D

- In the code review, I learned how to use Strategy pattern and Command pattern in homework.
- In the session report, the specific application scenarios of the Observer pattern were introduced. I learned a lot through collecting data and summarizing discussions.
- Through listening to reports from other groups, I also learned a lot about Strategy pattern and Command pattern.
- I learned some Code refactoring skills to help write more standardized code in the future.